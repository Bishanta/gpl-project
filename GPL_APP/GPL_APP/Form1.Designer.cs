﻿namespace GPL_APP
{
    partial class GplApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GplApplication));
            this.cmdLine = new System.Windows.Forms.TextBox();
            this.console = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFile = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syntaxCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminate = new System.Windows.Forms.ToolStripMenuItem();
            this.error_label = new System.Windows.Forms.Label();
            this.cmdText = new System.Windows.Forms.RichTextBox();
            this.fillcolor_label = new System.Windows.Forms.Label();
            this.copyright = new System.Windows.Forms.Label();
            this.btn_run = new System.Windows.Forms.Button();
            this.displayWindow = new System.Windows.Forms.PictureBox();
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdLine
            // 
            this.cmdLine.AccessibleName = "commandTextBox";
            this.cmdLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(40)))));
            this.cmdLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdLine.ForeColor = System.Drawing.Color.Lime;
            this.cmdLine.Location = new System.Drawing.Point(26, 353);
            this.cmdLine.Name = "cmdLine";
            this.cmdLine.Size = new System.Drawing.Size(245, 26);
            this.cmdLine.TabIndex = 1;
            this.cmdLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdLine_KeyDown);
            // 
            // console
            // 
            this.console.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(40)))));
            this.console.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.console.ForeColor = System.Drawing.Color.LimeGreen;
            this.console.Location = new System.Drawing.Point(24, 417);
            this.console.Multiline = true;
            this.console.Name = "console";
            this.console.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.console.Size = new System.Drawing.Size(318, 87);
            this.console.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(22, 394);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Console";
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(838, 28);
            this.menuBar.TabIndex = 5;
            this.menuBar.Text = "menuBar";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFile,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItem.Image")));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadFile
            // 
            this.loadFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.loadFile.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.loadFile.Image = ((System.Drawing.Image)(resources.GetObject("loadFile.Image")));
            this.loadFile.Name = "loadFile";
            this.loadFile.Size = new System.Drawing.Size(180, 24);
            this.loadFile.Text = "Load";
            this.loadFile.Click += new System.EventHandler(this.loadFile_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.saveToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.exitToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.syntaxCheckToolStripMenuItem,
            this.terminate});
            this.helpToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripMenuItem.Image")));
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.aboutToolStripMenuItem.Text = "Clear";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.MenuClearClick);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.resetToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.resetToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("resetToolStripMenuItem.Image")));
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.MenuResetClick);
            // 
            // syntaxCheckToolStripMenuItem
            // 
            this.syntaxCheckToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.syntaxCheckToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.syntaxCheckToolStripMenuItem.Image = global::GPL_APP.Properties.Resources.icon_load;
            this.syntaxCheckToolStripMenuItem.Name = "syntaxCheckToolStripMenuItem";
            this.syntaxCheckToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.syntaxCheckToolStripMenuItem.Text = "Syntax Check";
            this.syntaxCheckToolStripMenuItem.Click += new System.EventHandler(this.SyntaxCheckEvent);
            // 
            // terminate
            // 
            this.terminate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.terminate.ForeColor = System.Drawing.Color.White;
            this.terminate.Image = global::GPL_APP.Properties.Resources.icon_exit;
            this.terminate.Name = "terminate";
            this.terminate.Size = new System.Drawing.Size(177, 24);
            this.terminate.Text = "Terminate";
            this.terminate.Click += new System.EventHandler(this.TerminateProgramEvent);
            // 
            // error_label
            // 
            this.error_label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.error_label.ForeColor = System.Drawing.Color.Red;
            this.error_label.Location = new System.Drawing.Point(98, 394);
            this.error_label.Name = "error_label";
            this.error_label.Padding = new System.Windows.Forms.Padding(100, 0, 0, 0);
            this.error_label.Size = new System.Drawing.Size(244, 20);
            this.error_label.TabIndex = 8;
            this.error_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmdText
            // 
            this.cmdText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(29)))), ((int)(((byte)(40)))));
            this.cmdText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cmdText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.cmdText.Location = new System.Drawing.Point(26, 38);
            this.cmdText.Name = "cmdText";
            this.cmdText.Size = new System.Drawing.Size(316, 309);
            this.cmdText.TabIndex = 9;
            this.cmdText.Text = "";
            // 
            // fillcolor_label
            // 
            this.fillcolor_label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.fillcolor_label.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fillcolor_label.Location = new System.Drawing.Point(381, 520);
            this.fillcolor_label.Name = "fillcolor_label";
            this.fillcolor_label.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.fillcolor_label.Size = new System.Drawing.Size(457, 16);
            this.fillcolor_label.TabIndex = 10;
            this.fillcolor_label.Text = "Fill : ❌ / Color : Black";
            this.fillcolor_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // copyright
            // 
            this.copyright.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(40)))));
            this.copyright.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyright.Location = new System.Drawing.Point(-3, 520);
            this.copyright.Name = "copyright";
            this.copyright.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.copyright.Size = new System.Drawing.Size(457, 16);
            this.copyright.TabIndex = 10;
            this.copyright.Text = "Copyright @ Bishanta Bhattarai";
            this.copyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_run
            // 
            this.btn_run.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.btn_run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_run.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.btn_run.Image = ((System.Drawing.Image)(resources.GetObject("btn_run.Image")));
            this.btn_run.Location = new System.Drawing.Point(288, 353);
            this.btn_run.Name = "btn_run";
            this.btn_run.Size = new System.Drawing.Size(54, 26);
            this.btn_run.TabIndex = 3;
            this.btn_run.UseVisualStyleBackColor = true;
            this.btn_run.Click += new System.EventHandler(this.textCompile);
            // 
            // displayWindow
            // 
            this.displayWindow.BackColor = System.Drawing.Color.White;
            this.displayWindow.Location = new System.Drawing.Point(381, 38);
            this.displayWindow.Name = "displayWindow";
            this.displayWindow.Size = new System.Drawing.Size(433, 466);
            this.displayWindow.TabIndex = 2;
            this.displayWindow.TabStop = false;
            this.displayWindow.Paint += new System.Windows.Forms.PaintEventHandler(this.displayWindow_Paint);
            // 
            // GplApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(838, 536);
            this.Controls.Add(this.copyright);
            this.Controls.Add(this.fillcolor_label);
            this.Controls.Add(this.cmdText);
            this.Controls.Add(this.error_label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.console);
            this.Controls.Add(this.btn_run);
            this.Controls.Add(this.displayWindow);
            this.Controls.Add(this.cmdLine);
            this.Controls.Add(this.menuBar);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuBar;
            this.Name = "GplApplication";
            this.Text = " ";
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayWindow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox cmdLine;
        private System.Windows.Forms.PictureBox displayWindow;
        private System.Windows.Forms.Button btn_run;
        private System.Windows.Forms.TextBox console;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFile;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.Label error_label;
        private System.Windows.Forms.RichTextBox cmdText;
        private System.Windows.Forms.Label fillcolor_label;
        private System.Windows.Forms.Label copyright;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem syntaxCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminate;
    }
}

